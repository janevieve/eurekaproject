locals {
  project-name = "EUREKA"

  vpc_cidr = "10.20.30.0/24"

  vpc_tags = {
    "Name" = "${local.project-name}-VPC"
  }
  custom_tags = {
    "BusinessUnit" = "AVS",
    "CostCenter"   = "000000"
    "Environment"  = "DEV"
  }
  igw_tags = {
    "Name" = "${local.project-name}-IGW"
  }

  default_sg_tags = {
    "Name" = "${local.project-name}-VPC-DEFAULT-SG"
  }

  default_sg_ingress = [
    {
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      description     = "${local.project-name}-VPC-DEFAULT-SG "
      security_groups = data.aws_security_group.default.id
    }
  ]

  default_sg_egress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

}


data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}



module "vpc" {
  name                           = local.project-name
  source                         = "github.com/terraform-aws-modules/terraform-aws-vpc"
  cidr                           = local.vpc_cidr
  manage_default_security_group  = true
  default_security_group_ingress = local.default_sg_ingress
  default_security_group_egress  = local.default_sg_egress
  enable_dns_hostnames           = true
  azs                            = ["us-east-2a","us-east-2b"]
  public_subnets                 = ["10.20.30.0/25", "10.20.30.128/25"]
  vpc_tags                       = local.vpc_tags
  tags                           = local.custom_tags
  igw_tags                       = local.igw_tags
  default_security_group_tags    = local.default_sg_tags
}

