output "sub1_instances_public_ips" {
  description = "Public IPs assigned to the EC2 instance"
  value       = module.ec2_instance-subnet1.public_ip
}

output "sub2_instances_public_ips" {
  description = "Public IPs assigned to the EC2 instance"
  value       = module.ec2_instance-subnet2.public_ip
}

output "sube1_az" {
  description = "Sub1 instances Availability zones"
  value       =  module.ec2_instance-subnet1.availability_zone
}

output "sube2_az" {
  description = "Sub2 instances Availability zones"
  value       =  module.ec2_instance-subnet2.availability_zone
}

output "sube1_volumes" {
  description = "Sub1 instance Volumes"
  value       =  aws_ebs_volume.this_sub1.*.id
}

output "sube2_volumes" {
  description = "Sub2 instances volumes"
  value       =  aws_ebs_volume.this_sub2.*.id
}

output "Sub1_instance_ids" {
  description = "Sub1 Instance ids"
  value = module.ec2_instance-subnet1.id
}
output "Sub2_instance_ids" {
  description = "Sub2 Instance ids"
  value = module.ec2_instance-subnet2.id
}
